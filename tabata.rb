class Tabata

  def self.run!
    new.run!
  end

  def initialize
    unless RUBY_PLATFORM =~ /darwin/
      raise "I cannot run on anything other than a Mac.  Shorry."
    end
  end

  def run!
    countdown

    8.times do |i|
      say "Set number #{i + 1}"
      workout_countdown
      rest_countdown
    end

    say "all done."
  end

  private

  def countdown
    %w(three two one).each do |word|
      say(word)
      sleep 1
    end

    say "go!"
  end

  def say(words)
    `say '#{words}'`
  end

  def sleep_and_beep(number_of_beeps)
    sleep 1
    say "ta"
    #print "\a" * number_of_beeps
  end

  def workout_countdown
    17.times { sleep_and_beep(1) }
    3.times  { sleep_and_beep(3) }

    say "rest!"
  end

  def rest_countdown
    7.times { sleep_and_beep(1) }
    3.times { sleep_and_beep(3) }

    say "go!"
  end

end
